/* Classe Sommet
 * Cette classe permet de représenter les sommets d'un Graphe à partir d'une valeur, de coordonnées et d'un nom.
 * Il est également possible de calculer la distance entre deux points ainsi qu'afficher un point sur le terminal.
*/

#ifndef Sommet_hpp
#define Sommet_hpp

#include <string>
#include <iostream>

class Sommet {

private:

    ////////////////
    // Attributs //
    //////////////

    // Valeur représente le numéro du Sommet. Ce numéro est unique pour un même Graphe
    int valeur;

    // Coordonnées du points dans le plan
    int abs, ord;

    // Nom du point (utilisé pour les villes par exemple)
    std::string nom;

public:

    ////////////////////
    // Constructeurs //
    //////////////////

    // COnstructeur vide
    Sommet();

    // Avec une valeur [O(1)]
    // La position du Sommet est aléatoire { [0-612[, [0-792[ }
    Sommet(int valeur);

    // Avec une valeur et une position [O(1)]
    Sommet(int valeur, int abs, int ord);

    // Avec une valeur, une position et un nom [O(1)]
    Sommet(int valeur, int abs, int ord, std::string nom);

    ///////////////
    // Getteurs //
    /////////////

    // valeur [O(1)]
    int getValeur();

    // abs [O(1)]
    int getAbs();

    // ord [O(1)]
    int getOrd();

    // nom [O(1)]
    std::string getNom();

    ///////////////
    // Méthodes //
    /////////////

    // Calcule et retourne la distance euclidienne entre le sommet courant et le sommet passé en paramètre [O(1)]
    int distance(Sommet);
};

// Rédéfinition de l'opérateur << pour l'affichage console [O(1)]
// Affiche le Sommet sous la forme "valeur{abs*ord}"
std::ostream& operator<<(std::ostream& os,  Sommet);

#endif