/* Classe Graphe
 * Cette classe permet de représenter des Graphe de différentes façon
 * Ainsi que d'appliquer diverses méthodes vues en cours HLIN501
 * Le Graphe est représenté aussi bien par liste de Sommet et d'Arête que par liste de voisins afin de pouvoir utiliser la représentation la plus efficace pour chaque situation
*/

#ifndef graphe_hpp
#define graphe_hpp

#include "Arete.hpp"
#include "Sommet.hpp"
#include <vector>
#include <iostream>
#include <fstream> // ofstream
#include <string>

class Graphe {

private:

	////////////////
	// Attributs //
	//////////////

	// Réprésentation du Graphe //

	// le nombre de sommet du Graphe (n)
	int nb_sommet;

	// le nombre d'arêtes du Graphe (m)
	int nb_arete;

	// La liste des Sommets du Graphe
	std::vector<Sommet> sommets;
	
	// La liste des arêtes du Graphe
	std::vector<Arete> aretes;

	// Graphe par liste de voisins
  	std::vector<std::vector<Sommet> > voisins;

	// Composantes //

	// La liste des composantes connexes du Graphe
	// Deux Sommets sont dans une même composante connexe s'il existe une Arête entre ces deux Sommets
	// Dans un Graphe non-orienté
  	std::vector<Sommet> composantes;

	// Parcours //

	// La liste des pères mise à jour lors du parcours du Graphe
	std::vector<Sommet> pere;

	// La liste des niveaux mise à jour lors du parcours
	std::vector<int> niveau;

	// L'ordre de parcours des Sommets
	std::vector<int> ordre;

	// Début de viste d'un Sommet lors du parcours en profondeur
	std::vector<int> debut;

	// Fin de visite d'un Sommet lors du parcours en profondeur
	std::vector<int> fin;

	// Dijkstra //

	// La liste des pères mise à jour par l'algorithme de Dijkstra
	std::vector<Sommet> pere_dijkstra;

	// La liste des distances à la racine mise à jour par dijkstra
	std::vector<int> distanceRacine;

	// Floyd-Warshall //

	// La liste des distances entre chaque sommet du Graphe mise à jour par floyd-Warshall
	std::vector<std::vector<int> > distance;

	// La liste des chemins pour aller d'un sommet à un autre dans le Graphe mise à jour par floyd-Warshall
	std::vector<std::vector<Sommet> > chemin;

	// Fermeture Transitive //

	// La liste des fermetures transitives du Graphe mis à jour par le calcul de la fermeture transitive.
	// [x][y] = true ssi il existe un chemin orienté de x à y dans le Graphe
	std::vector<std::vector<bool> > fermeture;

public:

	////////////////////
    // Constructeurs //
	//////////////////

	// Graphe vide [O(1)]
	Graphe();

	// Graphe avec un certain nombre de Sommets générés aléatoirement, sans arêtes [O(n)]
	Graphe(int nb_sommet);

	// Graphe à partir d'une liste de Sommets, sans arêtes [O(n)]
	Graphe(std::vector<Sommet> sommets);

	// Graphe à partir d'une matrice d'adjacence représentant le poids des arêtes [O(n²)]
 	// Rajoute les arêtes orientées dans le graphe avec les poids demandés.
 	// Un poids INFINI représente une arête inexistante.
	// Ne représente pas les arêtes de x vers x de poids 0
	Graphe(std::vector<std::vector<int> > longueurs);

	// Graphe à partir d'une liste de Sommets et de longueur [O(n²)]
	Graphe(std::vector<Sommet> sommets, std::vector<std::vector<int> > longueurs);

	///////////////
	// Getteurs //
	/////////////

	// Tous n'y sont pas car inutile, utiliser les fonctions d'affichage
	// Ceux-ci sont là uniquement pour la redéfinition de l'opérateur <<
	std::vector<Arete> getAretes();

	int getNb_arete();

	/////////////////////
	// Initialisation //
	///////////////////

	// Initialise le Graphe en mettant les attributs nb_sommet et nb_arete à 0 et initialise les listes de sommets et arêtes à vide
	void init();

	///////////////////////////
    // Génération aléatoire //
	/////////////////////////

	// Génère n sommets aléatoires du Graphe, numérotés de 0 à n-1 [O(n)]
	void genererSommets(int n);

	// Génère m Arêtes, dont les Sommets sont choisis aléatoirement dans le Graphe, en autorisant les boucles (x-x) et les arêtes multiples (x-y, x-y, y-x) [O(m)]
	void genererAretesAvecBoucles(int m);

	// Génère un Graphe avec n Sommets et m Arêtes, le tout aléatoirement, en autorisant les boucles et les arêtes multiples [O(n²)]
	void genererGrapheAvecBoucles(int n, int m);

	// Génère un Graphe aléatoire sans boucle ni arêtes multiple avec n sommets et m arêtes [O(n²)]
	void genererGrapheVoisinsRandom(int n, int m);

	//////////////////////////////
    // Génération Conditionnée //
	////////////////////////////

	// Génère un Graphe en ajoutant toutes les arêtes respectant distance(x, y) <= distanceMax [O(n²)]
	void genererGrapheVoisinsDistance(int distanceMax);

	// Construit et retourne l'arbre des plus courts chemins calculé par dijkstra en construisant les arêtes en fonction des pere_dijkstra du Graphe courant. [O(n)]
	// Pré-requis : dijkstra()
	Graphe genererArbreDijkstra();

	///////////////////////////////////
	// Construction "petit à petit" //
	/////////////////////////////////

	// Ces constructions effectuent toutes les modifications necessaires (voisins, nb_arete, ...)

	// Ajoute une Arete non orientée entre les Sommets x et y [O(1)]
	void ajouterArete(Sommet x, Sommet y);

	// Ajoute une Arête orientée entre x et y de poids fixe [O(1)]
	void ajouterAreteOriente(Sommet x, Sommet y, int poids);

	// Ajoute une arete orientée entre les Sommets d'indice x et y de poids fixe [O(1)]
	void ajouterAreteOriente(int x, int y, int poids);

	/////////////////
	// Affichages //
	///////////////

	// sauf mention contraire, ces affichages sont effectués sur le terminal

	// Génère un fichier .ps représentant le Graphe courant [O(n+m)]
	// Pré-requis : nom doit être de la forme "fichier.ps"
	void affichageGraphique(std::string nom);

	// Affichage des composantes [O(n)]
	// Pré-requis : calculComposantes()
	void afficherComposantes();

	// Affichage de la taille de chaque composante du Graphe [O(n)]
	// Pré-requis : calculComposantes()
	void afficherTailleComposantes();

	// Affichage des poids de toutes les Arêtes [O(m)]
	void afficherPoids();

	// affiche les différents niveaux [O(n)]
	// Pré-requis : parcoursLargeur()
	void afficheNiveaux();

	// affiche la liste distance du Graphe [O(n²)]
	// Pré-requis : floydWarshall()
	void afficheDistance();

	// affiche la liste des chemins du Graphe courant [O(n²)]
	// Pré-requis : floydWarshall()
	void afficheChemin();

	// affiche le plus court chemin de x à y dans le Graphe orienté [O(n)]
	// Pré-requis : floydWarshall()
	void afficheItineraire(int x, int y);

	// affiche la liste fermeture du Graphe courant [O(n²)]
	// Pré-requis : fermetureTransitive()
	void afficherFermeture();

	// Calculle et affiche les composantes fortement connexes du Graphe [O(n²)]
	// x et y sont dans une même composante fortement connexe ssi il existe un chemin de x à y et un chemin de y à x dans le Graphe
	// Pré-requis : fermetureTransitive()
	void afficherCompFortConnexe();

	//////////////////
	// Composantes //
	////////////////

	// Calcul des composantes du Graphe [O(n²)]
	// Deux sommets x et y sont dans une même composante ssi il existe un chemin de x à y, dans un Graphe non orienté
	void calculComposantes();

	//////////////
	// Kruskal //
	////////////

	// Trie la liste des arêtes selon leur poids croissant [O(mlogm)]
	// Utilisé par kruskal()
	void trieAretes();

	// Calcule et retourne l'arbre couvrant de poids minimal selon l'algorithme de Kruskal [O(n*m)]
	Graphe kruskal();

	///////////////
	// Parcours //
	/////////////

	// Effectue le parcours en largeur du Graphe courant en mettant à jour les variables ordre, niveau et pere [O(n+m)]
	void parcoursLargeur();

	// Effectue le parcours en profondeur du Graphe courant en mettant à jour les variables ordre, debut, fin et pere [O(n+m)]
	void parcoursProfondeur();

	// crée un graphe à partir du Graphe courant, représentant l'arbre du parcours [O(n²)]
	// Pré-requis : parcours[Largeur/Profondeur]()
	Graphe grapheFromParcours();

	///////////////
	// Dijkstra //
	/////////////

	// applique l'algorithme de dijkstra sur le Graphe afin de calculer pere_dijkstra et distanceRacine [O(n²)]
	void dijkstra();

	/////////////////////
	// Floyd-Warshall //
	///////////////////

	// applique l'algorithme de floyd-warshall sur le Graphe courant en actualisant les listes distance et longueur [O(n³)]
	void floydWarshall();

	///////////////////////////
	// Fermeture Transitive //
	/////////////////////////

	// Calcule la fermeture transitive du Graphe orienté [O(n²)]
	// Met à jour la liste fermeture de telle sorte que fermeture[i][j] = true ssi il existe un chemin orienté de i à j dans le Graphe.
 	// Pré-requis : floydWarshall()
	void fermetureTransitive();
};

// Rédéfinition de l'opérateur << pour l'affichage console [O(1)]
// Affiche le Graphe sous la forme "[Graphe] : {x-y , x-z , z-y}"
std::ostream& operator<<(std::ostream& os,  Graphe);

#endif