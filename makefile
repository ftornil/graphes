CC=g++
CFLAGS=-Wall -std=c++14 -g

.cpp:
	$(CC) $(CFLAGS) -o $@  $<
.o:
	$(CC) $(CFLAGS) -o $@  $<

.cpp.o:
	$(CC) $(CFLAGS) -c $<

graphe: Graphe.o Graphe.hpp Arete.o Arete.hpp Sommet.o Sommet.hpp Main.o
	$(CC) $(CFLAGS) -o main Graphe.o Arete.o Sommet.o Main.o

all :  graphe

clean:
	rm *.o main *.ps
