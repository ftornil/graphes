# Graphe

Cet ensemble permet de représenter des Graphes

## Cadre

Ce travail a été réalisé par Florent Tornil dans le cadre de l'UE HLIN501 - Graphes

## Utilisation et Modifications

 Ce travail est mis à disposition de tous ceux qui le souhaitent.  
 Toutes nouvelles idées ou corrections sont les bienvenues.


## Suite des TP pour suivre l'avancement

### TP1 : La composante Géante
- [X] Création d'un graphe aléatoire par liste d'arêtes (boucle)
- [X] Calcul des composantes connexes
- [X] Ecriture des tailles des composantes
- [ ] Optimisation du calcul des composantes

### TP2 : Algorithme de Kruskal
- [X] Création des Sommets comme points du plan
- [X] Calcul des distances -- poids des arêtes
- [X] Tri des arêtes selon leur poids
- [X] Calcul de l'arbre couvrant de poids minimal selon l'algorithme de Kruskal
- [X] Ajouter l'affichage graphique

### TP3 : Arbre en largeur et en profondeur
- [X] Création aléatoire d'un arbre par liste de voisins (sans boucles)
- [X] Implémenter le parcours en largeur vu en cours
- [X] Afficher le niveau de chaque sommet
- [X] Implémenter le parcours en profondeur
- [X] Affichage graphique d'un arbre en largeur d'un Graphe avec poids sur les arêtes (distances)

### TP4 : Plus courts chemins
- [X] Création de points aléatoires
- [X] Construire la liste des voisins en fonction de la distance des points
- [X] Affichage graphique du Graphe
- [X] Implémenter l'algorithme de Dijkstra
- [X] Construire et affficher l'arbre à partir des pères définis par l'algorithme de dijkstra
- [ ] Aller plus loin ...

### TP5 : Plus courts chemins entre tous couples
- [ ] A quoi ça sert les graphes
- [X] Floyd-Warshall
- [X] Calcul des chemins
- [X] Calcul d'un itinéraire
- [X] Fermeture transitive
- [X] Composantes fortement connexes
- [ ] Pour aller plus loin ...

### En dehors
- [X] Calcul itinéraire réseau routier (5.4)
- [X] Constructeur de Graphe à partir d'une matrice de poids
- [X] Constructeur de Graphe à partir d'une matrice d'adjacence (5.5) (mettre des INFINI au lieu des 0 et constructeur via matrice de longueur)
- [X] Calculer les complexités