#include "Graphe.hpp"
#include <time.h>
#include <algorithm> // std::sort
#include <iomanip> // setw

#define INFINI 9999

// Constructeurs

Graphe::Graphe() {
	this->init();
}

Graphe::Graphe(int nb_sommet) {
	this->init();
	this->genererSommets(nb_sommet);
}

Graphe::Graphe(std::vector<Sommet> sommets) {
	this->init();
	this->sommets = sommets;
	this->nb_sommet = sommets.size();
	for(int i=0; i<this->nb_sommet; i++) {
		this->voisins.push_back({});
	}
}

Graphe::Graphe(std::vector<std::vector<int> > longueur) {
	this->init();
	int n = longueur.size();
	this->genererSommets(n);
	for(int i=0; i<n; i++) {
		for(int j=0; j<n; j++) {
			if(longueur[i][j] != INFINI && i != j && longueur[i][j] != 0) {
				this->ajouterAreteOriente(i, j, longueur[i][j]);
			}
		}
	}
}

Graphe::Graphe(std::vector<Sommet> sommets, std::vector<std::vector<int> > longueur) {
	this->init();
	this->sommets = sommets;
	this->nb_sommet = sommets.size();
	for(const Sommet s : this->sommets) {
		this->voisins.push_back({});
	}
	int n = longueur.size();
	for(int i=0; i<n; i++) {
		for(int j=0; j<n; j++) {
			if(longueur[i][j] != INFINI && i != j && longueur[i][j] != 0) {
				this->ajouterAreteOriente(i, j, longueur[i][j]);
			}
		}
	}
}

void Graphe::init() {
	this->nb_sommet = 0;
	this->nb_arete = 0;
	this->sommets = {};
	this->aretes = {};
}

void Graphe::genererSommets(int nb_sommet) {  
	this->sommets = {};
	for(int i=0; i<nb_sommet; i++) {
		this->sommets.push_back(Sommet(i));
		this->voisins.push_back({});
	}
	this->nb_sommet = nb_sommet;
}

void Graphe::genererAretesAvecBoucles(int nb_arete) {
	for(int i=0; i<nb_arete; i++) {
		Sommet x = this->sommets[rand()%nb_sommet];
		Sommet y = this->sommets[rand()%nb_sommet];
		this->ajouterArete(x, y);
	}
}

void Graphe::genererGrapheAvecBoucles(int nb_sommet, int nb_arete) {
	this->genererSommets(nb_sommet);
	this->genererAretesAvecBoucles(nb_arete);
}

void Graphe::genererGrapheVoisinsRandom(int nb_sommet, int nb_arete) {
	this->genererSommets(nb_sommet);
	while(this->nb_arete < nb_arete) {
		Sommet x = this->sommets[rand()%nb_sommet];
		Sommet y = this->sommets[rand()%nb_sommet];
		if(x.getValeur() != y.getValeur()) {
			bool trouve = false;
			for(Sommet s: this->voisins[x.getValeur()]) {
				trouve = trouve || s.getValeur() == y.getValeur();
			}
			if(!trouve) {
				this->ajouterArete(x, y);
			}
		}
	}  
}

void Graphe::genererGrapheVoisinsDistance(int dmax) {
	for(int i=0; i<this->nb_sommet; i++) {
		Sommet x = this->sommets[i];
		for(int j=i+1; j<this->nb_sommet; j++) {
			Sommet y = this->sommets[j];
			if(x.distance(y) <= dmax) {
				this->ajouterArete(x, y);
			}
		}
	}
}

void Graphe::calculComposantes() {
	this->composantes = {};
	for(int i = 0; i<this->nb_sommet; i++) {
		this->composantes.push_back(this->sommets[i]);
	}
	for (int i=0; i<this->nb_arete; i++) {
		int x = this->aretes[i].getX().getValeur();
		int y = this->aretes[i].getY().getValeur();
		if(this->composantes[x].getValeur() != this->composantes[y].getValeur()) {
			Sommet temp = this->composantes[x];
			for(int z = 0; z<this->nb_sommet; z++) {
				if(this->composantes[z].getValeur() == temp.getValeur()) {
					this->composantes[z] = this->composantes[y];
				}
			}
		}
	}	    
}

void Graphe::afficherComposantes() {
	for(Sommet s: this->sommets) {
		std::cout << "composante(" << s << ") = " << this->composantes[s.getValeur()] << std::endl;
	}
}

void Graphe::afficherTailleComposantes() {
	int compteComposantes[this->nb_sommet] = {};
	for(int i=0; i<this->nb_sommet; i++) {
		compteComposantes[this->composantes[i].getValeur()]++;
	}

	int compteTaille[this->nb_sommet+1] = {};
	for(int i=0; i<this->nb_sommet; i++) {
		compteTaille[compteComposantes[i]]++;
	}

	std::cout << "Il y a " << compteTaille[1] << " points isolés \n";

	for(int i=2; i<=this->nb_sommet; i++) {
		if(compteTaille[i] == 1) {
			std::cout << "Il y a 1 composante de taille " << i << "\n";
		}else if(compteTaille[i] > 1) {
			std::cout << "Il y a " << compteTaille[i] << " composantes de taille " << i << "\n";
		}
	} 
}

void Graphe::afficherPoids() {
	for(Arete a: this->aretes) {
		std::cout << "poid(" << a << ") = " << a.poids() << std::endl;
	}
}

void Graphe::trieAretes() {
	std::sort(this->aretes.begin(), this->aretes.end(), [](Arete a, Arete b){
		return a.poids() < b.poids();
	});
}

void Graphe::ajouterArete(Sommet x, Sommet y) {
	Arete a = Arete(x, y);
	this->voisins[x.getValeur()].push_back(y);
	this->voisins[y.getValeur()].push_back(x);
	this->aretes.push_back(a);
	this->nb_arete++;
}

Graphe Graphe::kruskal() {
	this->trieAretes();
	Graphe res = Graphe(this->sommets);
	int aux = 0;
	int comp[this->nb_sommet];
	for(int i=0; i<this->nb_sommet; i++) {
		comp[i] = i;
	}
	for (int i=0; i<this->nb_arete; i++) {
		Sommet x = this->aretes[i].getX();
		Sommet y = this->aretes[i].getY();
		if(comp[x.getValeur()] != comp[y.getValeur()]) {
			aux = comp[x.getValeur()];
			res.ajouterArete(x, y);
			for(int z=0; z<this->nb_sommet; z++) {
				if(comp[z] == aux) {
					comp[z] = comp[y.getValeur()];
				}
			}
		}	    
	}
	return res;
}

void Graphe::affichageGraphique(std::string nom) {
	std::ofstream output;                           
	output.open(nom, std::ios::out);
	output << "%!PS-Adobe-3.0" << std::endl;
	output << "%%BoundingBox: 0 0 612 792" << std::endl;
	output << std::endl;
	// affichage de la racine d'une autre couleur
	output << this->sommets[0].getAbs() << " " << this->sommets[0].getOrd() << " 4 0 360 arc" << std::endl;
	output << "1 0 0 setrgbcolor" << std::endl;
	output << "fill" << std::endl;
	output << "stroke"<< std::endl;
	output << std::endl;
	output << "0 setgray" << std::endl;
	for(int i=1; i<this->nb_sommet; i++) {
		output << this->sommets[i].getAbs() << " " << this->sommets[i].getOrd() << " 3 0 360 arc" << std::endl;
		output << "fill" << std::endl;
		output << "stroke"<< std::endl;
		output << std::endl;
	}
	output << std::endl;
	for(int i=0; i<this->nb_arete; i++) {
		output << this->aretes[i].getX().getAbs() << " " << this->aretes[i].getX().getOrd() << " moveto" << std::endl;
		output << this->aretes[i].getY().getAbs() << " " << this->aretes[i].getY().getOrd() << " lineto" << std::endl;
		output << "stroke" << std::endl;
		output << std::endl;
	}
	output << "showpage";
	output << std::endl;
}

void Graphe::parcoursLargeur() {
	this->pere = {};
	this->niveau = {};
	this->ordre = {};
	for(int i=0; i<this->nb_sommet; i++) {
		this->pere.push_back(NULL);
		this->niveau.push_back(-1);
		this->ordre.push_back(-1);
	}

	bool dv[this->nb_sommet] = {};
	Sommet racine = this->sommets[0];
	int indiceRacine = racine.getValeur();
	dv[indiceRacine] = true;
	ordre[indiceRacine] = 1;
	niveau[indiceRacine] = 0;
	pere[indiceRacine] = racine;

	std::vector<Sommet> aTraiter;
	aTraiter.push_back(racine);

	int temps = 2;

	while(!aTraiter.empty()) {
		Sommet sommetCourant = aTraiter[0];
		aTraiter.erase(aTraiter.begin());
		for(Sommet x: voisins[sommetCourant.getValeur()]) {
			int xValeur = x.getValeur();
			if(!dv[xValeur]) {
				dv[xValeur] = true;
				aTraiter.push_back(x);
				ordre[xValeur] = temps++;
				niveau[xValeur] = niveau[sommetCourant.getValeur()] +1;
				pere[xValeur] = sommetCourant;
			}
		}
	}
}

void Graphe::afficheNiveaux() {
	int compteur[this->nb_sommet] = {};
	int horsCompRacine = 0;
	for(int i=0; i<this->nb_sommet; i++) {
		int niv = this->niveau[i];
		if(niv < 0) {
			horsCompRacine++;
		} else {
			compteur[niv]++;
		}
	}

	std::cout << "Il y a 1 sommet au niveau 0." << std::endl;

	for(int i=1; i<this->nb_sommet; i++) {
		if(compteur[i] == 1) {
			std::cout << "Il y a 1 sommet au niveau " << i << "." << std::endl;
		}
		else if(compteur[i] > 1) {
			std::cout << "Il y a " << compteur[i] << " sommets au niveau " << i << "." << std::endl;
		}
	}
	if(horsCompRacine > 1) {
		std::cout << "Il y a " << horsCompRacine << " sommets qui ne sont pas dans la composante de 0" << std::endl; 
	}
	else if(horsCompRacine == 1) {
		std::cout << "Il y a 1 sommet qui n'est pas dans la composante de 0" << std::endl;
	}
	
}

void Graphe::parcoursProfondeur() {
	this->pere = {};
	this->niveau = {};
	this->ordre = {};
	this->debut = {};
	this->fin = {};
	for(int i=0; i<this->nb_sommet; i++) {
		this->pere.push_back(NULL);
		this->niveau.push_back(-1);
		this->ordre.push_back(-1);
		this->debut.push_back(-1);
		this->fin.push_back(-1);
	}

	bool dv[this->nb_sommet] = {};
	Sommet racine = this->sommets[0];
	int indiceRacine = racine.getValeur();
	dv[indiceRacine] = true;
	debut[indiceRacine] = 1;
	pere[indiceRacine] = racine;
	niveau[indiceRacine] = 0;

	std::vector<Sommet> aTraiter;
	aTraiter.push_back(racine);

	int temps = 2;

	while(!aTraiter.empty()) {
		Sommet sommetCourant = aTraiter.back();
		if(this->voisins[sommetCourant.getValeur()].size() == 0) {
			aTraiter.pop_back();
			this->fin[sommetCourant.getValeur()] = temps++;
		}
		else {
			Sommet y = this->voisins[sommetCourant.getValeur()].back();
			this->voisins[sommetCourant.getValeur()].pop_back();
			int yIndice = y.getValeur();
			if(!dv[yIndice]) {
				dv[yIndice] = true;
				aTraiter.push_back(y);
				debut[yIndice] = temps++;
				pere[yIndice] = sommetCourant;
				niveau[yIndice] = niveau[sommetCourant.getValeur()]+1;
			}
		}
	}
}

Graphe Graphe::grapheFromParcours() {
	Graphe res = Graphe(this->sommets);
	for(Sommet s : this->sommets) {
		Sommet y = this->pere[s.getValeur()];
		if(&y != NULL) {
			if(s.getValeur() != y.getValeur()) {
				res.ajouterArete(s, y);
			}
		}
	}
	return res;
}

void Graphe::dijkstra() {
	this->pere_dijkstra = {};
	this->distanceRacine = {};
	for(int i=0; i<this->nb_sommet; i++) {
		this->pere_dijkstra.push_back(NULL);
		this->distanceRacine.push_back(INFINI);
	}

	std::vector<bool> traite = {};
	for(Sommet s: this->sommets) {
		traite.push_back(false);
	}

	Sommet racine = this->sommets[0];
	this->pere_dijkstra[0] = racine;
	this->distanceRacine[0] = 0;

	// tant qu'il reste des sommets non traités
	while(std::find(traite.begin(), traite.end(), false) != traite.end()) {

		// on récupère le sommet qui a la plus faible distance à la racine et qui n'est pas traité
		int indicex = -1;
		bool existe = false;
		for(int i=0; !existe && i<this->nb_sommet; i++) {
			if(!traite[i]) {
				indicex = i;
				existe = true;
			}
		}
		for(int i=0; existe && i<this->nb_sommet; i++) {
			indicex = this->distanceRacine[i] < this->distanceRacine[indicex] && !traite[i] ? i : indicex ;
		}

		Sommet x = this->sommets[indicex];
		traite[indicex] = true;
		for(Sommet y: this->voisins[indicex]) {
			int distancery = this->distanceRacine[indicex] + x.distance(y);
			if(!traite[y.getValeur()] && distanceRacine[y.getValeur()] > distancery) {
				distanceRacine[y.getValeur()] = distancery;
				pere_dijkstra[y.getValeur()] = x;
			}
		}
	}
}

Graphe Graphe::genererArbreDijkstra() {
	Graphe arbre = Graphe(this->sommets);
	for(int i=0; i<this->nb_sommet; i++) {
		Sommet x = this->sommets[i];
		Sommet y = this->pere_dijkstra[i];
		arbre.ajouterArete(x, y);
	}
	return arbre;
}

void Graphe::ajouterAreteOriente(Sommet x, Sommet y, int poids) {
	Arete a = Arete(x, y, poids);
	this->voisins[x.getValeur()].push_back(y);
	this->aretes.push_back(a);
	this->nb_arete++;
}

void Graphe::ajouterAreteOriente(int x, int y, int poids) {
	Sommet xx = this->sommets[x];
	Sommet yy = this->sommets[y];
	this->ajouterAreteOriente(xx, yy, poids);
}

void Graphe::floydWarshall() {
	this->distance = {};
	this->chemin = {};
	this->fermeture = {};
	for(int i=0; i<this->nb_sommet; i++) {
		this->distance.push_back({});
		this->chemin.push_back({});
		this->fermeture.push_back({});
		for(int j=0; j<this->nb_sommet; j++) {
			this->distance[i].push_back(INFINI);
			this->chemin[i].push_back(NULL);
			this->fermeture[i].push_back(false);
		}
	}
	for(Arete a: this->aretes) {
		this->distance[a.getX().getValeur()][a.getY().getValeur()] = a.poids();
		this->chemin[a.getX().getValeur()][a.getY().getValeur()] = a.getY();
	}
	for(int i=0; i<this->nb_sommet; i++) {
		this->distance[i][i] = 0;
		this->chemin[i][i] = this->sommets[i];
	}

	for(int k=0; k<this->nb_sommet; k++) {
		for(int i=0; i<this->nb_sommet; i++) {
			for(int j=0; j<this->nb_sommet; j++) {
				if(this->distance[i][j] > this->distance[i][k] + this->distance[k][j]
				&& this->distance[i][k] != INFINI && this->distance[k][j] != INFINI) {
					this->distance[i][j] = this->distance[i][k] + this->distance[k][j];
					this->chemin[i][j] = this->chemin[i][k];
				}
			}
		}
	}

	for(int i=0; i<this->nb_sommet; i++) {
		if(this->distance[i][i]<0) {
			std::cout << "Il existe un cycle orienté de poids < 0" << std::endl;
		}
	}
}

void Graphe::afficheDistance() {
	for(std::vector<int> v : this->distance) {
		for(int i: v) {
			std::cout << std::setw(3);
			i==INFINI? std::cout << "inf" : std::cout << i;
			std::cout << " * ";
		}
		std::cout << std::endl;
	}
}

void Graphe::afficheChemin() {
	for(std::vector<Sommet> v : this->chemin) {
		for(Sommet i: v) {
			std::cout << std::setw(3);
			&i ==NULL? std::cout << "imp" : std::cout << i.getValeur();
			std::cout << " * ";
		}
		std::cout << std::endl;
	}
}

void Graphe::afficheItineraire(int dep, int arr) {
	if(dep == arr) {
		std::cout << arr << std::endl;
	} else {
		if(&this->chemin[dep][arr] == NULL) {
			std::cout << "Chemin Impossible" << std::endl;
		} else {
			std::cout << dep << " ";
			this->afficheItineraire(this->chemin[dep][arr].getValeur(), arr);
		}
	}
}

void Graphe::fermetureTransitive() {
	for(int i=0; i<this->nb_sommet; i++) {
		for(int j=0; j<this->nb_sommet; j++) {
			this->fermeture[i][j] = &this->chemin[i][j] != NULL;
		}
	}
}

void Graphe::afficherFermeture() {
	for(int i=0; i<this->nb_sommet; i++) {
		for(int j=0; j<this->nb_sommet; j++) {
			std::cout << this->fermeture[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

void Graphe::afficherCompFortConnexe() {
	std::cout << "Les composantes fortement connexes sont : ";
	std::vector<bool> dv = {};
	for(int i=0; i<this->nb_sommet; i++) {
		dv.push_back(false);
	}
	for(int i=0; i<this->nb_sommet; i++) {
		if(!dv[i]) {

			if(i!=0) {
				std::cout << ", ";
			}

			std::cout << "{";
			bool fst = true;
			for(int j=0; j<this->nb_sommet; j++) {
				if(this->fermeture[i][j] && this->fermeture[j][i]) {
					if(!fst) {
						std::cout << ",";
					}
					fst = false;
					std::cout << j;
					dv[j] = true;
				}
			}
			std::cout << "}";
		}
	}
	std::cout << std::endl;
}

std::vector<Arete> Graphe::getAretes() {
	return this->aretes;
}

int Graphe::getNb_arete() {
	return this->nb_arete;
}

std::ostream& operator<<(std::ostream& os,  Graphe g) {
	os << "[Graphe] : {";
	std::vector<Arete> ar = g.getAretes();
	int nb_ar = g.getNb_arete();
	for(int i=0; i<nb_ar; i++) {
		os << ar[i];
		if(i < nb_ar-1) {
			os << " , ";
		}
	}
	return os << "}" << std::endl;
}