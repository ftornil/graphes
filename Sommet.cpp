#include "Sommet.hpp" // .hpp
#include <cmath> // sqrt + pow

// Constructeurs

Sommet::Sommet() {}

Sommet::Sommet(int valeur) {
    this->valeur = valeur;
    this->abs = rand()%612;
    this->ord = rand()%792;
    this->nom = "";
}

Sommet::Sommet(int val, int abs, int ord) {
    this->valeur = val;
    this->abs = abs;
    this->ord = ord;
    this->nom = "";
}

Sommet::Sommet(int val, int abs, int ord, std::string nom) {
    this->valeur = val;
    this->abs = abs;
    this->ord = ord;
    this->nom = nom;
}

// Getteurs

int Sommet::getValeur() {
    return this->valeur;
}

int Sommet::getAbs() {
    return this->abs;
}

int Sommet::getOrd() {
    return this->ord;
}

std::string Sommet::getNom() {
    return this->nom;
}

// Méthodes

// Calcul de la distance
int Sommet::distance(Sommet s) {
    return sqrt(pow(this->abs - s.abs, 2) + pow(this->ord - s.ord, 2));
}

// Opérateur <<
std::ostream& operator<<(std::ostream& os,  Sommet s) {
    return os << s.getValeur() << "{" << s.getAbs() << "*" << s.getOrd() << "}";
}