#include "Arete.hpp" // .hpp

// Constructeurs

Arete::Arete() {}

Arete::Arete(Sommet x, Sommet y) {
    this->x = x;
    this->y = y;
    this->poid = 0;
}

Arete::Arete(Sommet x, Sommet y, int p) {
    this->x = x;
    this->y = y;
    this->poid = p;
}

// Getteurs

Sommet Arete::getX() {
    return this->x;
}

Sommet Arete::getY() {
    return this->y;
}

// Méthodes

int Arete::poids() {
    return this->poid == 0? this->x.distance(this->y) : this->poid;
}

// Opérateur <<
std::ostream& operator<<(std::ostream& os,  Arete a) {
    return os << a.getX() << "-" << a.getY();
}