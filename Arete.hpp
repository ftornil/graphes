/* Classe Arete
 * Cette classe permet de représenter les arêtes d'un Graphe à partir des deux Sommet incidents ainsi qu'un poids.
 * Il est également possible d'afficher une arête sur le terminal.
*/

#ifndef arete_hpp
#define arete_hpp

#include "Sommet.hpp"
#include <iostream>

class Arete{

private:

    ////////////////
    // Attributs //
    //////////////

    // Un sommet de l'arête
    // Si l'arête est orientée, x est le Sommet duquel part l'arête
    Sommet x;

    // Le deuxième sommet de l'arête
    // Si l'arête est orientée, y est le Sommet auquel arrive l'arête
    Sommet y;

    // Poid de l'arête ssi celui-ci est défini dans le constructeur
    // Utiliser la fonction poids() comme getteur !
    // Attention pour les Arête de poids fixe 0 !
    int poid;

public:

    ////////////////////
    // Constructeurs //
    //////////////////
	
	// Constructeur vide
	Arete();

    // A partir des deux Sommets [O(1)]
    Arete(Sommet, Sommet);

    // A partir des deux Sommets mais avec un poid prédéfini [O(1)]
    Arete(Sommet, Sommet, int poids);

    ///////////////
    // Getteurs //
    /////////////

    // x [O(1)]
    Sommet getX();

    // y [O(1)]
    Sommet getY();

    ///////////////
    // Méthodes //
    /////////////

    // Calcule et retourne le poids de l'arête
    // Ce poids correspond au poids défini lors de la construction de l'arête s'il est défini et différent de 0. Sinon il correspond à la distance euclidienne entre les deux Sommets x et y
    int poids();
};

// Rédéfinition de l'opérateur << pour l'affichage console [O(1)]
// Affiche l'Arête sous la forme "x-y"
std::ostream& operator<<(std::ostream& os,  Arete);

#endif